package application;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import http.HeaderAttribute;
import http.HttpException;
import http.HttpRequest;
import http.HttpRequestMethod;
import http.HttpResponse;

public class PageRequest {
	
	/**
	 * Represents a website resource and contains the http response
	 */
	private class Resource{
		public Resource(){};
		public Resource(String url, String path, int port)
			{this.url = url; this.path = path; this.port = port;}
		public String url;
		public String path;
		public int port;
		public HttpResponse context;
	}
	
	
	/**
	 * contains all http connections, these connections can be in open or closed state
	 */
	private Map<String,HttpConnection> connections;
	
	
	/**
	 * Holds the main page for whom resources are being fetched
	 */
	private Resource mainPage;
	
	
	/**
	 * Holds the resource handler specifically oriented towards the url map 
	 */
	private LocalResourceHandler resourceHandler;
	
	public PageRequest(String url, int port, String path) throws HttpException{
		connections = new HashMap<String,HttpConnection>();
		connections.put(url,new HttpConnection(url,port));
		resourceHandler = new LocalResourceHandler(url);
		mainPage = new Resource(url, path, port);
	}
	
	
	/**
	 * call this function to fetch the main page and it's resources, returns the main page httpResponse
	 * 
	 * @throws Exception
	 * 			| Exception is thrown when it fails to fetch the main page or one of it's resources
	 *  		| check the logs for more information
	 */
	public HttpResponse fetchPage() throws Exception{
		fetchResourceContext(mainPage);
		List<Resource> resources = parseHtmlForResources(new String(mainPage.context.getBody(), "UTF-8"));
		resources.forEach( x -> { 
				if(!connections.containsKey(x.url))
					connections.put(x.url, new HttpConnection(x.url, x.port));
				fetchResourceContext(x);
		});
		connections.values().forEach(x->x.closeConnection());
		return mainPage.context;
	}
	
	/**
	 * Fetch a specific resource, opens a connection and saves the response in the resource context. 
	 * The fetched file is also saved locally. The file is not fetched if it is already kept in cache.
	 * 
	 * @param resource the resource to be fetched
	 */
	private void fetchResourceContext(Resource resource){
		try {
			HttpConnection connection = getActiveConnection(resource.url);
			if(connection == null) return;
			HttpRequest request = new HttpRequest(HttpRequestMethod.GET, resource.path ,new HeaderAttribute("Host", resource.url + ":"+ resource.port),
					new HeaderAttribute("If-Modified-Since", formatDate(resourceHandler.getLastModified(resource.path))));
			resource.context = connection.sendRequest(request);
				
			if(resource.context != null && resource.context.getBody().length > 0)
				resourceHandler.SaveResourceLocally(resource.context, resource.path);
		} catch (Exception e) {
			System.err.println("Failed to fetch resource: " + resource.url + resource.path + "\n" + e.getMessage());
		}
			
	}
	
	
	/**
	 * Get a connection by url, if the connection is closed or not opened yet, the connection will be opened
	 * 
	 * @param keyUrl the url linked to the connection
	 * @return Returns the active connection, returns null if the connection does not exist yet
	 */
	private HttpConnection getActiveConnection(String keyUrl){
		HttpConnection connection = null;
		if(connections.containsKey(keyUrl)){
			connection = connections.get(keyUrl);
			if(!connection.isOpen())
				connection.openConnection();
		}
		return connection;
	}
	
	
	/**
	 * Parse a html file into a list of resources
	 * 
	 * @param HTML the html file in string format
	 * @return returns a list of resources for whom the path, url and port is set
	 */
	private List<Resource> parseHtmlForResources(String HTML){
		String regex = "(src=|SRC=)\"([a-zA-Z0-9|_|-|.|?|=|&]+)([^\">]+)";

		 // Create a Pattern object
	      Pattern patern = Pattern.compile(regex);

	      // Now create matcher object.
	      Matcher m = patern.matcher(HTML);
	      
	      List<Resource> allResources = new ArrayList<Resource>();
	      while (m.find()) {
	    	  try {
		    	  String match = m.group().substring(m.group().indexOf("\"")+1,m.group().length());
		    	  allResources.add(parseUrl(match));
			} catch (Exception e) {
				System.out.println("failed to fetch resource: " + m.group());
			}

	      }
	      return allResources;
	}
	
	
	/**
	 * Parse a url into a path, url and port section. if Port is not given, default port 80 will be used.
	 * 
	 * @param url full url, eg. www.google.be/index.html
	 * @return return a new resource with the path, port and url
	 */
	private Resource parseUrl(String url){
		Resource resource = new Resource();
		if(isAbsoluteUrl(url)){
			URL aUrl;
			try {
				aUrl = new URL(url);
				resource.path = aUrl.getPath();
				resource.url = "www." + aUrl.getHost();
				resource.port = 80;
			} catch (MalformedURLException e) {
				resource.path = "";
				resource.url = "";
				resource.port = 80;
			}
			
		}
		else{
			url = url.startsWith("/")? "" : "/" + url;
			resource.url = mainPage.url;
			resource.path = url;
			resource.port = mainPage.port;
		}
		return resource;
	}
	
	/**
	 * Checks if given url is an absolute url or a relative path
	 *  
	 * @param url to be checked
	 * @return returns if the url is absolute or not
	 * 			|true if url contains www, http, ...
	 * 			|false in any other situation
	 */
	private boolean isAbsoluteUrl(String url){
		if(url.contains("www.") || url.contains("http://") || url.contains(".com")|| url.contains(".be")|| url.contains(".net"))
			return true;
		return false;
	}
	
	/**
	 * format date to http header format
	 * @param date to format
	 * @return date in header format, as string
	 */
	private static String formatDate(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		return dateFormat.format(date);
	}
}
