package application;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import http.HttpResponse;

public class LocalResourceHandler {
	
	/**
	 * Root directory containing all cached data
	 */
	private static String root = "cache";
	
	public LocalResourceHandler(String rootFolder) {
		root = root + "/" + rootFolder;
	}
	
	/**
	 * Saves the resource in the designated path
	 * 
	 * @param response contains the data that is to be saved
	 * @param Path represents the path to the resource, this path should contain the file name
 			  | Root path / is saved as index.html 
	 * @throws IOException
	 * 		  | Thrown if file can not be saved, invalid path or name is not given
	 */
	public void SaveResourceLocally(HttpResponse response, String path) throws IOException{
		if(path.equals("/")) 
			path += "index.html"; 
		writeBinaryImage(response.getBody(), root + path);
	}
	
	
	/**
	 * Writes the data as binary content to designated path 
	 * 
	 * @param content binary content to be saved
	 * @param path Path represents the path to the resource, this path should contain the file name
	 * @throws IOException
	 * 		   | Thrown if file can not be saved, invalid path or name is not given	
	 */
	private void writeBinaryImage(byte[] content, String path) throws IOException{
		Path pathToFile = Paths.get(path);
		if(pathToFile.getParent() != null){
			Files.createDirectories(pathToFile.getParent());
		}
		
		FileOutputStream fos = new FileOutputStream(path, false);
		fos.write(content);
		fos.close();
	}
	
	/**
	 * Return the the date the file was last modified in relative path
	 * @param relativePath
	 * @return date
	 * @throws DataBaseException
	 * 		   | Thrown if filetype could not be determined
	 */
	public Date getLastModified(String path){
		try {
			Path pathToFile = Paths.get(root + path);
			return new Date(Files.getLastModifiedTime(pathToFile).toMillis());
		} catch (IOException e) {
			return new Date(0l);
		}
	}
}
