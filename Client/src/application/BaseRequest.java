package application;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.invoke.MethodType;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import http.HeaderAttribute;
import http.HttpException;
import http.HttpRequest;
import http.HttpRequestMethod;
import http.HttpResponse;

/**
 * Handles all basic http methods: PUT, POST, HEAD
 * @author Balder Croquet
 */
public class BaseRequest {	
	
	
	/**
	 * Opens the http connection and sends the request based on given data
	 * @param Method the method of the http request
	 * 		  |only PUT,POST,HEAD are supported
	 * @param url the url of the web server
	 * @param port the port to access
	 * @param path the path of the file that has to be accessed 
	 * @param message the message to be send by put and post
	 * 		  | not used for HEAD request
	 * @throws HttpException 
	 * 		  |thrown if server refuses connection or if invalid data is entered
	 */
	public void PostAndPutRequests(HttpRequestMethod method, String url, int port, String path, String message) throws HttpException{
		HttpRequest request = null;
		switch (method) {
		case PUT:
			request = new HttpRequest(HttpRequestMethod.PUT, path ,new HeaderAttribute("Host", url + ":"+ port),
					new HeaderAttribute("Content-Length", Integer.toString(message.getBytes().length)),new HeaderAttribute("Connection:", "close"));
			request.setBody(message.getBytes());
			break;
		case POST:
			request = new HttpRequest(HttpRequestMethod.POST, path ,new HeaderAttribute("Host", url + ":"+ port),
					new HeaderAttribute("Content-Length", Integer.toString(message.getBytes().length)),new HeaderAttribute("Connection:", "close"));
			request.setBody(message.getBytes());
			break;
		case HEAD:
			request = new HttpRequest(HttpRequestMethod.HEAD, path ,new HeaderAttribute("Host", url + ":"+ port),
					new HeaderAttribute("Content-Length", Integer.toString(0)),new HeaderAttribute("Connection:", "close"));
			break;
		default:
			throw new UnsupportedOperationException(method.name() + ": Not supported in base request");
		}

		try {
			HttpConnection connection = new HttpConnection(url, port);
			connection.openConnection();
			HttpResponse response = connection.sendRequest(request);
			connection.closeConnection();
		} catch (IOException e) {
			System.err.println("something went wrong with the connection: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
