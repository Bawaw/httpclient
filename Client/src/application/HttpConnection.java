package application;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;

import http.HttpException;
import http.HttpRequest;
import http.HttpResponse;
import http.HttpMessageFactory;

public class HttpConnection {
	private Socket socket;
	private final String url;
	private final int port;
	
	/**
	 * object used to send data to the server
	 */
	private DataOutputStream out;
	
	/**
	 * object used to read data send by the server
	 */
	private InputStream in;
	
	
	public HttpConnection(String uri, int port){
		this.url = uri;
		this.port = port;
	}
	
	public void openConnection(){
		try {
			socket = new Socket(url, port);
			out = new DataOutputStream(socket.getOutputStream());
			in = socket.getInputStream();
		} catch (Exception e) {
			System.err.println("Something failed in opening the connection: \n" + e.getMessage() );
		}
	}
	
	public HttpResponse sendRequest(HttpRequest request) throws HttpException, IOException{
	    out.write(request.getHyperTextRepresentation());
	    
	    ByteArrayOutputStream  data = new ByteArrayOutputStream();

	    byte[] buf = new byte[1024];
	    
	    int n;
	    HttpResponse response = null;
	    int splitIndex = -1;
	    long contentLength = -1;
	    while ( (n = in.read(buf)) > 0) 
	    {
	    	data.write(buf, 0, n);
	    	if(response == null && (splitIndex = HttpMessageFactory.findSplitIndex(data.toByteArray())) != -1){
	    		response = HttpMessageFactory.createHttpResponseHeaderFromByteStream(data.toByteArray());
	    		if(response.valueOfAttribute("Content-Length") == null)
	    			contentLength = 1;
	    		else
	    			contentLength = Long.parseLong(response.valueOfAttribute("Content-Length"));
	    	}
	    	if(response != null && data.size() - splitIndex >= contentLength){
	    		if(contentLength == 0)
	    			response.setBody(new byte[0]);
	    		else
	    			response.setBody(Arrays.copyOfRange(data.toByteArray(), splitIndex+3, data.toByteArray().length-1));
	    		//TODO: remove empty space instead of 
	    		System.out.println(new String(response.getBody(), "UTF-8"));
	    		break;
	    	}
	    		
	    }
	    return response;
	}
	
	public void closeConnection(){
		try {
			socket.close();
		} catch (Exception e) {
			System.err.println("There were some problems closing the connection with: " + url);
		}
	    
	}
	
	public boolean isOpen(){
		return socket != null && socket.isConnected() && !socket.isClosed();
	}
	
}
