package application;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.ServerException;
import java.util.HashMap;
import java.util.UUID;

/**
 * Responsible for listening on the server port and creating new session threads.
 * TODO: if thread no longer running, remove it from active sessions
 * @author gamin
 *
 */
public class SocketListener {
	private static final int SERVER_PORT = 8080;
	private static final int MAXIMUM_ITERATIONS = 200000;
	
	private HashMap<String, Session> activeSessions = new HashMap<String, Session>();
	ServerSocket serverSocket;
	
	public SocketListener() throws IOException {
		serverSocket = new ServerSocket(SERVER_PORT);
	}
	
	/**
	 * Starts the server, listen on port for incoming connections and delegate to new thread objects.
	 * @throws HttpServerException
	 * @throws IOException
	 */
	public void run() throws HttpServerException, IOException{
		System.out.println("Launching server, listening on port: " + serverSocket.getLocalPort());
	    while (true) {
	        Socket clientSocket = serverSocket.accept();
	        Session clientSession = new Session(clientSocket, generateUniqueId());
	        clientSession.start();
	    }
	}
	
	/**
	 * Generate a unique identifier to distinguish each new session
	 * 
	 * @return string  unique identifier
	 * @throws HttpServerException
	 */
	private String generateUniqueId() throws HttpServerException{
		int countr = 0;
		while (true && countr < MAXIMUM_ITERATIONS) {
			String uniqueID = UUID.randomUUID().toString();
			if(!activeSessions.containsKey(uniqueID))
				return uniqueID;
			countr ++;
		}
		throw new HttpServerException("Server seems to be saturated");
	}
	
	public static void main(String[] args) throws HttpServerException, IOException {
		new SocketListener().run();
	}

}
