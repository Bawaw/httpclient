package application;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import database.DataBaseException;
import database.DataManager;
import http.ByteSteamHandler;
import http.HeaderAttribute;
import http.HttpException;
import http.HttpMessageFactory;
import http.HttpRequest;
import http.HttpRequestMethod;
import http.HttpResponse;
import http.HttpStatusCode;

/**
 * responsible for managing each client session, the session remains open until the client sends
 * a Connection: close or the proces has been idle for to long
 * @author Balder Croquet
 *
 */
public class Session extends Thread {
	private static final String SERVER_NAME = "Bawaw/0.1";
	private static final int MAX_IDLE_TIME = Integer.MAX_VALUE; //10000;
	
	/**
	 * Holds the socket connection
	 */
	private final Socket clientSocket;
	
	/**
	 * Holds the client identifier
	 */
	private final String clientID;
	
	
	private HttpMessageFactory httpFactory = new HttpMessageFactory();
	
	/**
	 * Holds if this session is still being used
	 */
	private boolean running = true;
	
	/**
	 * Object through which data can be accessed and manipulated
	 */
	DataManager dm = new DataManager();

	public Session(Socket clientSocket, String clientID) throws SocketException {
		this.clientSocket = clientSocket;
		clientSocket.setSoTimeout(MAX_IDLE_TIME);
		this.clientID = clientID;
		dm = new DataManager();
	}

	/**
	 * Listens to socket until time out or connection is closed.
	 * Read requests and delegate correct method based on the request method.
	 */
	public void run() {
		System.out.println("New session launched, id: " + clientID);
		try {
			InputStream in = clientSocket.getInputStream(); //data stream from client to server
			DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream()); //data stream from server to client

			int n = 0;
			while (running) {
				ByteArrayOutputStream data = new ByteArrayOutputStream();
				byte[] buf = new byte[1024];	//buffer to be filled with data
				n = in.read(buf);				//fill buffer with read data
				if (n < 0) break; 
				data.write(buf, 0, n);
				HttpRequest request = httpFactory.createHttpRequestFromByteStream(data.toByteArray());	//create a request from the received byte stream
				
				HttpResponse response = null;
				switch (request.getRequestMethod()) {
				case GET:
					response = handleFetchRequest(request);
					break;
				case HEAD:
					response = handleFetchRequest(request);
					response.setBody(new byte[0]);
					break;
				case POST:
				case PUT: 
					handleWriteRequest(request);
					break;
				default:
					response = new HttpResponse(HttpStatusCode.SERVER_ERROR,
									new HeaderAttribute("Content-Length", Integer.toString(0)),
									new HeaderAttribute("Date", formatDate(Calendar.getInstance().getTime())));
					break;
				}
				if(response != null)
					out.write(response.getHyperTextRepresentation());
				
				//if the client has asked to close the connection or it's an http 1.0 connection close the connection
				if(request.isCloseRequest() || request.getHttpVersion().equals(HttpRequest.HTTP_VERSION_1_0))	
					running = false;
			}
			
			System.out.println("Closing connection: " + clientID);
			clientSocket.close();
		}catch (SocketTimeoutException ste) {
			System.out.println("Connection timed out, closing: " + clientID);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		closeConnection();
	}
	
	private void closeConnection(){
		try {
			clientSocket.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Handles the GET requests, handles file fetching and the creation of the httpResponse.
	 * 
	 * @param request httpRequest to be handled by this method
	 * @return httpResponse with valid file data
	 * @throws HttpException 
	 * 		   |thrown if httpResponse fails to be created by some internal error
	 */
	private HttpResponse handleFetchRequest(HttpRequest request) throws HttpException{
		HttpResponse response;
		String serverTime = formatDate(Calendar.getInstance().getTime());

		try {
			String modifiedDate = request.valueOfAttribute("If-Modified-Since");
			
			//checks if the file has been modified, if it hasn't send not modified response
			if(modifiedDate != null &&  dm.getLastModified(request.getPath()).before(parseDate(modifiedDate)) ){
				response = new HttpResponse(HttpStatusCode.NOT_MODIFIED, request.getHttpVersion(),
						new HeaderAttribute("Content-Length", Integer.toString(0)),
						new HeaderAttribute("Date", serverTime));
			}
			
			//fetch the file and return it as a response
			else
			{
				byte[] content = dm.ReadFile(request.getPath());
				response = new HttpResponse(HttpStatusCode.OK, request.getHttpVersion(),content, new HeaderAttribute("Date", serverTime),
					new HeaderAttribute("Content-Type", dm.getFileType(request.getPath())),
					new HeaderAttribute("Content-Length", Integer.toString(content.length)),
					new HeaderAttribute("Connection", "Keep-Alive"),
					new HeaderAttribute("Last-modified", formatDate(dm.getLastModified(request.getPath()))));
			}
			
			
		} 
		
		//if the file couldn't be fetched throw not found exception
		catch (DataBaseException e) {
			response = new HttpResponse(HttpStatusCode.NOT_Found, request.getHttpVersion(),
					new HeaderAttribute("Content-Length", Integer.toString(0)),
					new HeaderAttribute("Date", serverTime));
		} 
		
		//if response generation failed for any other reason throw a server error
		catch (Exception e) {
			System.err.println(e.getMessage());
			response = new HttpResponse(HttpStatusCode.SERVER_ERROR,
					new HeaderAttribute("Content-Length", Integer.toString(0)),
					new HeaderAttribute("Date", serverTime));
		}

		response.addHeaderAttribute(new HeaderAttribute("Server", SERVER_NAME));
		return response;
	}
	
	/**
	 * Handles the Write requests, handles file writing and the creation of the httpResponse.
	 * 
	 * @param request httpRequest to be handled by this method
	 * @return httpResponse with valid file data
	 * @throws HttpException 
	 * 		   |thrown if httpResponse fails to be created by some internal error
	 */
	private void handleWriteRequest(HttpRequest request) throws HttpException{
		try {
			byte[] classifier = (request.getRequestMethod().toString() + " data received \t Date: " + formatDate(Calendar.getInstance().getTime()) + "\t Id: " + clientID + "\n").getBytes();
			byte[] text = new byte[classifier.length + request.getBody().length + 3];
			Arrays.fill(text,(byte)'\n');
			dm.WriteFile(ByteSteamHandler.mergeByteStreams(text, classifier, request.getBody()));
			
		} catch (DataBaseException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * format date to http header format
	 * @param date to format
	 * @return date in header format, as string
	 */
	private static String formatDate(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		return dateFormat.format(date);
	}
	
	/**
	 * parse date http header format to Date
	 * @param date in header format, as string 
	 * @return date as java object
	 * @throws ParseException 
	 */
	private static Date parseDate(String date) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		return dateFormat.parse(date);
	}

}
