package application;

/**
 * Exception that is to be thrown if something goes wrong with the handling of client connections
 * @author Balder Croquet
 *
 */
public class HttpServerException extends Exception{
	private static final long serialVersionUID = 1L;
	
	public HttpServerException(String message) {
		super(message);
	}
}
