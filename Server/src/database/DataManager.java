package database;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;

/**
 * Responsible for handling all local content data.
 * @author Balder Croquet
 *
 */
public class DataManager {
	
	/**
	 * the root page on GET /
	 */
	private static String root = "index.html";
	
	/**
	 * the datbase filepath
	 */
	private String dataFilePath = "/Database/Data.dat";
	
	/**
	 * The webcontent directory
	 */
	private static String rootDirectory;
	
	
	/**
	 * Create a new Datamanager object with Default webContent in \\Content\\webPage_1\\
	 * 
	 */
	public DataManager(){
		try {
			rootDirectory = new File(".").getCanonicalPath() + "\\Content\\webPage_1\\";
			dataFilePath = new File(".").getCanonicalPath() + dataFilePath;
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
	
	/**
	 * Reform the relative paths to absolute paths in the webcontent directory
	 * 
	 * @param relativePath
	 * @return absolutePath
	 */
	private Path handlePathRedirects(String localPath){
		Path path;
		if(localPath.equals("/"))
			path = Paths.get(rootDirectory + root);
		else 
			path = Paths.get(rootDirectory + localPath);
		return path;
	}
	
	/**
	 * Reads the binary file data in relative path
	 * 
	 * @param relativePath
	 * @return file contents as a byte array
	 * @throws DataBaseException 
	 * 		   | Thrown if handler failes to read the given file 
	 */
	public byte[] ReadFile(String relativePath) throws DataBaseException{
		try {
			return Files.readAllBytes(handlePathRedirects(relativePath));
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new DataBaseException("Failed to read file: " + relativePath);
		}
	}
	
	/**
	 * Write the binary data to file in relative path
	 * 
	 * @param relativePath
	 * @return file contents as a byte array
	 * @throws DataBaseException 
	 * 		   | Thrown if handler fails to write to the given file 
	 */
	public void WriteFile(byte[] data) throws DataBaseException{
		try {
			Files.write(Paths.get(dataFilePath), data, StandardOpenOption.APPEND);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new DataBaseException("Failed to write file: " + dataFilePath);
		}
	}
	
	/**
	 * Returns the file type in relative path
	 * 
	 * @param relativePath
	 * @return fileType
	 * @throws DataBaseException
	 * 		   | Thrown if filetype could not be determined
	 */
	public String getFileType(String relativePath) throws DataBaseException {
		try {
			return Files.probeContentType(handlePathRedirects(relativePath));
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new DataBaseException("Could not determine filetype: " + relativePath);
		}
	}
	
	/**
	 * Return the the date the file was last modified in relative path
	 * @param relativePath
	 * @return date
	 * @throws DataBaseException
	 * 		   | Thrown if filetype could not be determined
	 */
	public Date getLastModified(String relativePath) throws DataBaseException {
		try {
			return new Date(Files.getLastModifiedTime(handlePathRedirects(relativePath)).toMillis());
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new DataBaseException("Could not determine filetype: " + relativePath);
		}
	}
}
