package http;

public enum HttpRequestMethod {
	GET, PUT, POST, HEAD
}
