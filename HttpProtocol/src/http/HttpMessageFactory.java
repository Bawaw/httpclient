package http;

import java.util.ArrayList;
import java.util.Arrays;

public class HttpMessageFactory {
	private static final byte NEW_LINE = (byte) '\n';
	private static final byte CARRIAGE_RETURN = (byte) '\r';
	
	/**
	 * Find the split index of header and body
	 * 
	 * @param response the http message as byte array
	 * @return return the array index were header ends
	 */
	public static int findSplitIndex(byte[] response){
		int index = -1;
		for (int i = 0; i < response.length; i++)
			if(i + 2 < response.length && response[i] == NEW_LINE && response[i+1] == CARRIAGE_RETURN && response[i+2] == NEW_LINE){
				return i;
			}	
		return index;
	}
	
	/**
	 * create a httpResponse header from byte stream
	 * @param response as byte array
	 * @return response object response with empty body
	 * @throws HttpException
	 */
	public static HttpResponse createHttpResponseHeaderFromByteStream(byte[] response) throws HttpException{
		
		try{
			int splitIndex = findSplitIndex(response);

			byte[] headerBytes = Arrays.copyOfRange(response, 0, splitIndex);
			//byte[] body = Arrays.copyOfRange(response, splitIndex+9, response.length-1);

			
			//---------------------Parsing header data---------------------//
			String[] header = new String(headerBytes, "UTF-8").split("\n");
			
			//eg: HTTP/1.1 200 OK
			String[] firstLine = header[0].split(" ");
			String httpVersion = firstLine[0];
			HttpStatusCode responseCode = HttpStatusCode.getFromCode(Integer.parseInt(firstLine[1]));
			//parse attributes
			ArrayList<HeaderAttribute> attributes = new ArrayList<HeaderAttribute>();
			for (int i = 1; i < header.length; i++) {
				String[] attribute = header[i].split(":",2);
				HeaderAttribute ha = new HeaderAttribute(attribute[0], attribute[1].substring(1, attribute[1].length()).replaceAll("\r", "").replaceAll("\n", ""));
				attributes.add(ha);
			}
			
			HeaderAttribute[] array = new HeaderAttribute[attributes.size()];
			//String bodyString = new String(body);
			//System.out.println(bodyString);
			
			//check if a response has a body
			return new HttpResponse(responseCode, httpVersion,(HeaderAttribute[]) attributes.toArray(array));
		} catch (Exception e) {
			throw new HttpException("Invalid http response format, failed to parse");
		}
	}
	
	/**
	 * Creates HttpRequest based on the request byte array
	 * 
	 * @param request as byte array
	 * @return request as object including body
	 * @throws HttpException
	 */
	public static HttpRequest createHttpRequestFromByteStream(byte[] request) throws HttpException{	
		try{
			int splitIndex = 0;
			for (int i = 0; i < request.length; i++)
				if(i + 2 < request.length && request[i] == NEW_LINE && request[i+1] == CARRIAGE_RETURN && request[i+2] == NEW_LINE){
					splitIndex = i; 
					break;
				}	
			byte[] headerBytes = Arrays.copyOfRange(request, 0, splitIndex);
			
			while(splitIndex < request.length && (request[splitIndex] == NEW_LINE || request[splitIndex] == CARRIAGE_RETURN))
				splitIndex ++;
			byte[] body = null;
			if(splitIndex < request.length)
				body = Arrays.copyOfRange(request, splitIndex, request.length);

			
			//---------------------Parsing header data---------------------//
			String[] header = new String(headerBytes, "UTF-8").split("\n");
			
			//eg: GET / HTTP/1.1
			String[] firstLine = header[0].split(" ");
			HttpRequestMethod method = HttpRequestMethod.valueOf(firstLine[0]);
			String httpVersion = firstLine[2].substring(0, firstLine[2].length()).replaceAll("\n","").replaceAll("\r","");
			String path = firstLine[1].replaceAll("\n","").replaceAll("\r","");
			//parse attributes
			ArrayList<HeaderAttribute> attributes = new ArrayList<HeaderAttribute>();
			for (int i = 1; i < header.length; i++) {
				String[] attribute = header[i].split(":",2);
				HeaderAttribute ha = new HeaderAttribute(attribute[0], attribute[1].substring(1, attribute[1].length()).replaceAll("\r", "").replaceAll("\n", ""));
				attributes.add(ha);
			}
			
			HeaderAttribute[] array = new HeaderAttribute[attributes.size()];
			
			if(body == null)
				return new HttpRequest(method, httpVersion, path, attributes.toArray(array));
			return new HttpRequest(method, httpVersion, path,body, attributes.toArray(array));
		} catch (Exception e) {
			throw new HttpException("Invalid http response format, failed to parse");
		}
	}
	
}
