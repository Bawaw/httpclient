package http;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represents the abstraction of both a HttpRequest as a HttpResponse
 * @author Bawaw
 *
 */
public abstract class HttpMessage implements IHyperTextObject{
	public static String HTTP_VERSION_1_1 = "HTTP/1.1";
	public static String HTTP_VERSION_1_0 = "HTTP/1.0";
	
	private String HttpVersion = HTTP_VERSION_1_1;
	
	/**
	 * holds all http Header attributes
	 */
	private List<HeaderAttribute> attributes;
	
	
	/**
	 * Holds the http body as a byte array 
	 */
	private byte[] body;
	
	protected HttpMessage(byte[] body, HeaderAttribute ...attributes){
		this.attributes = new ArrayList<HeaderAttribute>(Arrays.asList(attributes));
		setBody(body);
	}
	
	protected HttpMessage(byte[] body, String httpVersion, HeaderAttribute ...attributes){
		this.attributes = new ArrayList<HeaderAttribute>(Arrays.asList(attributes));
		setBody(body);
		setHttpVersion(httpVersion);
	}
	
	
	/**
	 * Add a http header attribute to the list of header attributes
	 * 
	 * @param headerAttribute the attribute to be added to the header
	 * @throws HttpException
	 * 		   | Http exception is thrown if the header already exists
	 */
	public void addHeaderAttribute(HeaderAttribute headerAttribute) throws HttpException{
		if(attributes.contains(headerAttribute))
			throw new HttpException("Header already assigned");
		attributes.add(headerAttribute);
	}
	
	public void setBody(byte[] body) {
		this.body = body;
	}
	
	public byte[] getBody() {
		return body;
	}
	
	/**
	 * Get the http version of the http message
	 * 
	 * @return Http version; 1.0 and 1.1 supported
	 */
	public String getHttpVersion() {
		return HttpVersion;
	}
	
	public void setHttpVersion(String httpVersion) {
		HttpVersion = httpVersion;
	}
	
	/**
	 * Get the value of an attribute based on the attributes name, note that this function is case sensitive
	 * 
	 * @param attributeName the name of the attribute
	 * @return the value of the http header attribute
	 */
	public String valueOfAttribute(String attributeName){
		for (HeaderAttribute headerAttribute : attributes)
			if(headerAttribute.getAttributeName().equals(attributeName))
				return headerAttribute.getValue();
		return null;
	}
	
	/**
	 * Check if the header contains the Connection:close header attribute
	 * 
	 * @return true if contains connection:close header attribute
	 */
	public boolean isCloseRequest(){
		String value = valueOfAttribute("Connection");
		return value == null? false : (value.equals("close") || value.equals(" close"));
	}
	
	
	/**
	 * Get the http message in the expected http format as a byte string
	 */
	@Override
	public byte[] getHyperTextRepresentation() {
		ArrayList<byte[]> attributeBytes = new ArrayList<byte[]>();
		attributes.forEach(x -> attributeBytes.add(x.getHyperTextRepresentation()));
		attributeBytes.add(("\r\n").getBytes());
		attributeBytes.add(getBody());
		return ByteSteamHandler.flattenByteArrayList(attributeBytes);
	}
	
}
