package http;

/**
 * represents a http header attribute, the attribute consists of a key and a value
 * @author Balder Croquet
 *
 */
public class HeaderAttribute implements IHyperTextObject{
	private final String attributeName;
	private final String value; 
	
	public HeaderAttribute(String attributeName, String value) {
		this.attributeName = attributeName;
		this.value = value;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public String getValue() {
		return value;
	}
	
	
	/**
	 * returns the header attribute in the expected http format
	 */
	public byte[] getHyperTextRepresentation(){
		return (attributeName + ": " + value + "\n").getBytes();
	}
}
