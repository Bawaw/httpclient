package http;

public interface IHyperTextObject {
	byte[] getHyperTextRepresentation();
}
