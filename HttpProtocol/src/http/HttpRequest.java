package http;

public class HttpRequest extends HttpMessage{
	private final HttpRequestMethod requestMethod;
	private String path = "/";
	
	
	public HttpRequest(HttpRequestMethod requestMethod, String httpVersion, String path, byte[] body, HeaderAttribute ...attributes) throws HttpException {
		super(body,httpVersion,attributes);
		this.requestMethod = requestMethod;
		setPath(path);	
	}
	
	public HttpRequest(HttpRequestMethod requestMethod, String path, byte[] body, HeaderAttribute ...attributes) throws HttpException {
		super(body,attributes);
		this.requestMethod = requestMethod;
		setPath(path);	
	}
	
	public HttpRequest(HttpRequestMethod requestMethod, String httpVersion, String path, HeaderAttribute ...attributes) throws HttpException {
		this(requestMethod, attributes);
		setHttpVersion(httpVersion);
		setPath(path);	
	}
	
	public HttpRequest(HttpRequestMethod requestMethod, String path, HeaderAttribute ...attributes) throws HttpException {
		this(requestMethod, attributes);
		setPath(path);	
	}
	
	public HttpRequest(HttpRequestMethod requestMethod, HeaderAttribute ...attributes) {
		super(new byte[0], attributes);
		this.requestMethod = requestMethod;
	}
	
	public void setPath(String relativePath) throws HttpException{
		if(!relativePath.startsWith("/"))
			throw new HttpException("Invalid uri: " + relativePath);
		this.path = relativePath;
	}
	
	public String getPath() {
		return path;
	}
	
	public HttpRequestMethod getRequestMethod() {
		return requestMethod;
	}
	
	/**
	 * Get the http request in the expected http format as a byte string
	 */
	@Override
	public byte[] getHyperTextRepresentation() {
		byte[] stream1 = (requestMethod.name() + " " + path + " " + getHttpVersion() + "\n").getBytes();
		byte[] stream2 = super.getHyperTextRepresentation();
		byte[] newStream = new byte[stream1.length + stream2.length];
		return ByteSteamHandler.mergeByteStreams(newStream, stream1, stream2);
	}
}
