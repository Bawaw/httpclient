package http;

import java.util.ArrayList;

/**
 * Class contains handy tools to work with byte arrays
 * @author Balder Croquet
 *
 */
public class ByteSteamHandler {
	
	/**
	 * Merge two byte arrays into a third byte array
	 * 
	 * @param newStream contains the merged data
	 * @param stream1 the first byte array
	 * @param stream2 the secont byte array
	 * @return
	 */
	public static byte[] mergeByteStreams(byte[] newStream, byte[] stream1, byte[] stream2){
		for (int i = 0; i < stream1.length; i++) 
			newStream[i] = stream1[i];
		for (int i = 0; i < stream2.length; i++) 
			newStream[stream1.length + i] = stream2[i];
		return newStream;
	}
	
	
	/**
	 * Flattens a list of Byte arrays and returns a single byte array
	 * @param attributeBytes list of byte arrays
	 * @return a single byte array containing all byte arrays of the list concatonated
	 */
	public static byte[] flattenByteArrayList(ArrayList<byte[]> attributeBytes){
		int length = attributeBytes.stream().mapToInt(x -> x.length).sum();
		byte[] newArray = new byte[length];
		int index = 0;
		for (int i = 0; i < attributeBytes.size(); i++)
			for (int j = 0; j < attributeBytes.get(i).length; j++) {
				newArray[index] = (attributeBytes.get(i))[j];
				index++;
			}
		return newArray;
	}
	
}
