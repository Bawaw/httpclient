package http;

public class HttpResponse extends HttpMessage{
	private HttpStatusCode responseCode;
	
	public HttpResponse(HttpStatusCode responseCode, String httpVersion, byte[] body, HeaderAttribute ...attributes) {
		super(body,attributes);
		setResponseCode(responseCode);	
		setHttpVersion(httpVersion);
	}
	
	public HttpResponse(HttpStatusCode responseCode, byte[] body, HeaderAttribute ...attributes) {
		super(body,attributes);
		setResponseCode(responseCode);	
	}
	
	public HttpResponse(HttpStatusCode responseCode, String httpVersion, HeaderAttribute ...attributes){
		this(responseCode,httpVersion,new byte[0],attributes);
	}
	
	public HttpResponse(HttpStatusCode responseCode, HeaderAttribute ...attributes){
		this(responseCode,new byte[0],attributes);
	}
	
	private void setResponseCode(HttpStatusCode responseCode) {
		this.responseCode = responseCode;
	}
	
	/**
	 * returns the status code of this response, can be 200, 304, 404, 500
	 * @return status code
	 */
	public HttpStatusCode getResponseCode() {
		return responseCode;
	}
	
	/**
	 * Get the http response in the expected http format as a byte string
	 */
	public byte[] getHyperTextRepresentation() {
		byte[] stream1 = (getHttpVersion() + " " + responseCode.getCode() + " " + responseCode.getTextRepresentation() + "\n").getBytes();
		byte[] stream2 = super.getHyperTextRepresentation();
		byte[] newStream = new byte[stream1.length+stream2.length];
		return ByteSteamHandler.mergeByteStreams(newStream, stream1, stream2);
	}
}
