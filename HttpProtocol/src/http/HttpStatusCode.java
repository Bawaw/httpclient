package http;

public enum HttpStatusCode {
	OK("OK",200), NOT_Found("Not Found",404), SERVER_ERROR("Server Error",500), NOT_MODIFIED("Not Modified",304);
	
	private final int code;
	private final String textRepresentation;
	
	private HttpStatusCode(String textRepresentation, int code) {
		this.code = code;
		this.textRepresentation = textRepresentation;
	}
	
	public int getCode() {
		return code;
	}
	
	public String getTextRepresentation() {
		return textRepresentation;
	}
	
	public static HttpStatusCode getFromCode(int code){
		
		switch (code) {
		case 200:
			return HttpStatusCode.OK;
		case 304:
			return HttpStatusCode.NOT_MODIFIED;
		case 404:
			return HttpStatusCode.NOT_Found;
		case 500:
			return HttpStatusCode.SERVER_ERROR;
		default:
			return HttpStatusCode.SERVER_ERROR;
		}
	}
}
